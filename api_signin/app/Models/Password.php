<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Password extends Model
{
    protected $table = "passwords";
    public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable = [
        'password',
        'email_id'
    ];

    public function email(){
        return $this->belongsTo('App\Models\Email');
    }

    /**
     * Generate password token for request new password.
     * @param $email_str
     * @return string
     */
    public static function generatePasswordToken($email_str){
        $email = Email::where('email', '=', $email_str)->first();
        $password = $email->password()->first();
        if($email === null OR $password === null){
            return false;
        }
        $rand_number = random_int(0, 255) + random_int(0,255) * random_int(0, 255) - random_int(0, 255);
        $rand_number_hash = md5($rand_number);
        $token_str = $password->password . $email->email . env('APP_KEY') . $rand_number_hash;
        return md5($token_str);
    }

}
