<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Password_Token extends Model
{
    public $timestamps = false;
    protected $table = 'passwords_tokens';
    protected $fillable = ['Token', 'emails_id'];
    protected $guarded = ['id'];

    public function email(){
        return $this->hasOne('App\Models\Email');
    }


}
