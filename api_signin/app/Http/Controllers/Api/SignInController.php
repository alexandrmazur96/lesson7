<?php

namespace App\Http\Controllers\Api;

use App;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\Models\User;
use App\Models\Email;
use App\Models\Password;
use App\Services\MailService;


/**
 * Проверяет авторизован-ли пользователь
 * Auth::check();
 *
 * Возвращает авторизованного пользователя или null
 * Auth::user();
 */

use Auth;

class SignInController extends Controller
{
    /**
     * @email string
     * @passwor string
     *
     *
     * @responce = {
     * "session":$hash,
     * "expire":$time,
     * "first_name":$firstName,
     * "last_name":$lastName
     * }
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function auth(Request $request)
    {
        $requestParams = $request->only(
            'email',
            'password'
        );

        $requestValidationParams = [
            'email' => 'required|email|min:6',
            'password' => 'required|min:8'
        ];

        $requestValidator = Validator::make(
            $requestParams,
            $requestValidationParams
        );

        if ($requestValidator->fails()) {
            return response('Invalid params passed', 400);
        }

        $pre_session_key = User::generateSessionKey($requestParams['email']);

        $hasToken = Cache::get($pre_session_key);

        if ($hasToken) {
            return response('User logged', 302);
        }

        $email = Email::where('email', '=', $requestParams['email'])->first();
        if ($email === null) {
            return response('Access forbidden', 403);
        }
        $password = $email->password()->first();

        if (Hash::check($requestParams['password'], $password->password)) {
            $user = User::find($email->user_id);
            $session_key = User::generateSessionKey($email->email);
            $expiresAt = Carbon::now()->addMinutes(120);
            $cacheData = [
                'id' => $email->user_id,
                'email' => $email->email,
                'name_first' => $user->first_name,
                'name_last' => $user->last_name,
                'session_key' => $session_key
            ];
            Cache::put($session_key, $cacheData, $expiresAt);
            $cache = Cache::get($session_key);
            return response($cache, 200);
        } else {
            return response('Access forbidden', 403);
        }
    }
//
//    private function checkPasswordToken($token)
//    {
////        return Password_Token::where('Token', '=', $token) === 1;
//        dd(Cache::get($token));
//        return Cache::has($token);
//    }

    public function set_password_by_token(Request $request)
    {
        $requestParams = $request->only(
            'token',
            'password'
        );

        $requestValidation = [
            'password' => 'required|min:8',
            'token' => 'required|min:32|max:32'
        ];
        $requestValidator = Validator::make(
            $requestParams,
            $requestValidation
        );
        if ($requestValidator->fails()) {
            return response('Invalid params passed', 403);
        }

        if (Cache::has($requestParams['token'])) {
            $password = Email::find(
                Password_Token::where('Token', '=', $requestParams['token'])
                    ->first()->emails_id)
                        ->password()->get();
            $password->password = bcrypt($requestParams['password']);
            $password->save();
        } else {
            return response('Invalid token were passed', 403);
        }
    }

    public function request_password_reset(Request $request)
    {
        $requestParam = $request->only('email');
        $requestParamValidation = [
            'email' => 'required|email|min:6|unique:emails,email'
        ];
        $requestParamValidator = Validator::make(
            $requestParam,
            $requestParamValidation
        );

        if (!$requestParamValidator->fails()) {
            return response('Access forbidden', 403);
        }

        $email_id_hash = md5($requestParam['email'] . env('APP_KEY'));
        if(Cache::has($email_id_hash)){
            $passwordToken = Cache::get($email_id_hash);
            echo 'cache has token = ', $passwordToken;
        } else {
            $passwordToken = Password::generatePasswordToken($requestParam['email']);
            $expiresAt = Carbon::now()->addMinutes(180);
            Cache::add($email_id_hash, $passwordToken, $expiresAt);
            echo 'generate token = ', $passwordToken;
        }

        (new MailService())->sendPasswordRequestMail($requestParam['email'], $passwordToken);
        return response('Reset message was requested', 200);
    }

//    public function auth(Request $request){
//        $a_validator = Validator::make($request->all(), [
//            'email' => 'required|min:5|max:255|email',
//            'password' => 'required:min:8'
//        ]);
//
//        if($a_validator->fails()){
//            http_response_code(403);
//            $error_return_value = [
//                'code' => http_response_code(),
//                'message' => 'Access Forbidden'
//            ];
//
//            return json_encode($error_return_value);
//        }
//
//        http_response_code(200);
//
//        $return_value = [
//            'code' => http_response_code(),
//            'message' => 'OK',
//            'id' => '',
//            'email' => '',
//            'name_first' => '',
//            'name_last' => '',
//            'session' => [
//                'id' => '',
//                'expires_at' => ''
//            ]
//        ];
//
////        ->find($email)
////        ->where('name', 'Jow');
////        ->where('name', '=', 'Jow');
////        ->whereNull
//
//        $email = new App\Email();
//        $user = new App\Users();
//        $password = new App\Password();
//
//        echo $test = $email->where('Email', '=', $request->input('email'))->where('IsMain', '=', 1)->count();
//    }
}
