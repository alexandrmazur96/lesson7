<?php

namespace App\Http\Controllers\Api;

use App;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\Models\User;
use App\Models\Email;

class SessionController extends Controller
{

    public function checkSession(Request $request)
    {
        $session_id = $request->input('session_id');
        if(Cache::has($session_id)){
            return response('Session live', 200);
        }else{
            return response('Session expired', 200);
        }
    }

    public function expire(Request $request)
    {
        $session_id = $request->input('session_id');

        if($session_id === null){
            return response('Authorization required', 401);
        }

        if(Cache::has($session_id)){
            Cache::forget($session_id);
            return response('Session were expired', 200);
        }
        else {
            return response('Access forbidden', 403);
        }
    }
}
