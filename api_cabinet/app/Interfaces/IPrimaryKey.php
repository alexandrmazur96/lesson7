<?php
/**
 * Created by PhpStorm.
 * User: alexandrmazur
 * Date: 27.07.17
 * Time: 14:58
 */

namespace App\Interfaces;


interface IPrimaryKey
{
    public function getId():int;
}