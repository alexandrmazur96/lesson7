<?php
/**
 * Created by PhpStorm.
 * User: alexandrmazur
 * Date: 27.07.17
 * Time: 15:05
 */

namespace App\Interfaces;

interface IUserSession extends IPrimaryKey
{
    public function user() : IUser;
    public function getExpiresAt() : int;
    public function prolong() : bool;
    public function expire() : bool;
}