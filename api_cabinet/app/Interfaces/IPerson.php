<?php
/**
 * Created by PhpStorm.
 * User: alexandrmazur
 * Date: 27.07.17
 * Time: 15:01
 */

namespace App\Interfaces;


interface IPerson extends IBookHolder, IPrimaryKey
{
    public function getNameFirst() : string;
    public function getNameLast() : string;
}