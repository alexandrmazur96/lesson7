<?php
/**
 * Created by PhpStorm.
 * User: alexandrmazur
 * Date: 27.07.17
 * Time: 15:06
 */

namespace App\Interfaces;


interface IBook extends IPrimaryKey
{
    public function getName() : string;
    public function getBookAuthors() : array;
    public function getISBN() : string;
    public function getDescription() : string;
    public function getYearOfPublication() : int;
    public function getFormat() : int;
    public function getPagesCount() : int;
}