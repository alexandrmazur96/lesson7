<?php
/**
 * Created by PhpStorm.
 * User: alexandrmazur
 * Date: 17.07.17
 * Time: 17:08
 */

namespace core\book;

use core\IBook;

class Book implements IBook
{

    public function getName(): string
    {
        // TODO: Implement getName() method.
    }

    public function getAuthors(): array
    {
        // TODO: Implement getAuthors() method.
    }

    public function getISBN(): string
    {
        // TODO: Implement getISBN() method.
    }

    public function getDescription(): string
    {
        // TODO: Implement getDescription() method.
    }

    public function getYearOfPublication(): int
    {
        // TODO: Implement getYearOfPublication() method.
    }

    public function getFormat(): int
    {
        // TODO: Implement getFormat() method.
    }

    public function getPagesCount(): int
    {
        // TODO: Implement getPagesCount() method.
    }

    public function getId(): int
    {
        // TODO: Implement getId() method.
    }
}