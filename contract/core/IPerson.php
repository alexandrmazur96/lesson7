<?php
/**
 * Created by PhpStorm.
 * User: alexandrmazur
 * Date: 17.07.17
 * Time: 16:13
 */

namespace core;

interface IPerson extends IPrimaryKey, IBookHolder
{
    public function getNameFirst() : string;

    public function getNameLast() : string;
}