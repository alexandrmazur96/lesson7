<?php
/**
 * Created by PhpStorm.
 * User: alexandrmazur
 * Date: 17.07.17
 * Time: 16:41
 */

namespace core;


/**
 * Class APerson
 * @package core
 */
abstract class APerson implements IPerson
{
    /**
     * @return array
     */
    abstract public function getBooks() : array;

    /**
     * @return string
     */
    abstract public function getNameFirst() : string;

    /**
     * @return string
     */
    abstract public function getNameLast() : string;

    /**
     * @return int
     */
    abstract public function getId() : int;
}