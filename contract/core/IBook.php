<?php
/**
 * Created by PhpStorm.
 * User: alexandrmazur
 * Date: 17.07.17
 * Time: 16:30
 */

namespace core;


interface IBook extends IPrimaryKey
{
    public function getName() : string;

    public function getAuthors() : array;

    public function getISBN() : string;

    public function getDescription() : string;

    public function getYearOfPublication() : int;

    public function getFormat() : int;

    public function getPagesCount() : int;
}