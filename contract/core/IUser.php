<?php
/**
 * Created by PhpStorm.
 * User: alexandrmazur
 * Date: 17.07.17
 * Time: 16:17
 */

namespace core;


interface IUser extends IPerson
{
    public function getEmail() : string;

    public function getSession() : IUserSession;
}