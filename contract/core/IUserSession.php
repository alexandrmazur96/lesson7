<?php
/**
 * Created by PhpStorm.
 * User: alexandrmazur
 * Date: 17.07.17
 * Time: 16:24
 */

namespace core;


interface IUserSession extends IPrimaryKey
{
    public function getUser() : IUser;

    public function getExpiresAt() : int;

    public function prolong(int $by) : bool;

    public function expire() : bool;
}