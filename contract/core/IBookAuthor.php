<?php
/**
 * Created by PhpStorm.
 * User: alexandrmazur
 * Date: 17.07.17
 * Time: 16:28
 */

namespace core;


interface IBookAuthor extends IPerson
{
    public function isAuthor() : bool;

    public function getUser() : IUser;
}