<?php
/**
 * Created by PhpStorm.
 * User: alexandrmazur
 * Date: 17.07.17
 * Time: 16:12
 */

namespace core;


interface IPrimaryKey
{
    public function getId() : int;
}