<?php
/**
 * Created by PhpStorm.
 * User: alexandrmazur
 * Date: 17.07.17
 * Time: 16:13
 */

namespace core;


interface IBookHolder
{

    public function getBooks() : array;

}