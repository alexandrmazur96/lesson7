<?php
/**
 * Created by PhpStorm.
 * User: alexandrmazur
 * Date: 17.07.17
 * Time: 17:25
 */

namespace core\person;


use core\APerson;
use core\IBookAuthor;
use core\IUser;

class Author extends APerson implements IBookAuthor
{

    /**
     * @return array
     */
    public function getBooks(): array
    {
        // TODO: Implement getBooks() method.
    }

    /**
     * @return string
     */
    public function getNameFirst(): string
    {
        // TODO: Implement getNameFirst() method.
    }

    /**
     * @return string
     */
    public function getNameLast(): string
    {
        // TODO: Implement getNameLast() method.
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        // TODO: Implement getId() method.
    }

    public function isAuthor(): bool
    {
        // TODO: Implement isAuthor() method.
    }

    public function getUser(): IUser
    {
        // TODO: Implement getUser() method.
    }
}