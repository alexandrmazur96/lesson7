<?php
/**
 * Created by PhpStorm.
 * User: alexandrmazur
 * Date: 17.07.17
 * Time: 17:24
 */

namespace core\person;


use core\IUser;
use core\IUserSession;

class UserSession implements IUserSession
{

    public function getId(): int
    {
        // TODO: Implement getId() method.
    }

    public function getUser(): IUser
    {
        // TODO: Implement getUser() method.
    }

    public function getExpiresAt(): int
    {
        // TODO: Implement getExpiresAt() method.
    }

    public function prolong(int $by): bool
    {
        // TODO: Implement prolong() method.
    }

    public function expire(): bool
    {
        // TODO: Implement expire() method.
    }
}