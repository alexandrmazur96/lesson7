<?php
/**
 * Created by PhpStorm.
 * User: alexandrmazur
 * Date: 17.07.17
 * Time: 17:11
 */

namespace core\person;

use core\APerson;
use core\IUser;
use core\IUserSession;

class User extends APerson implements IUser
{
    /**
     * @return array
     */
    public function getBooks(): array
    {
        // TODO: Implement getBooks() method.
    }

    /**
     * @return string
     */
    public function getNameFirst(): string
    {
        // TODO: Implement getNameFirst() method.
    }

    /**
     * @return string
     */
    public function getNameLast(): string
    {
        // TODO: Implement getNameLast() method.
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        // TODO: Implement getId() method.
    }

    public function getEmail(): string
    {
        // TODO: Implement getEmail() method.
    }

    public function getSession(): IUserSession
    {
        // TODO: Implement getSession() method.
    }
}