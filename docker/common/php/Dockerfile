FROM php:7.1-fpm

RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y \
    apt-utils \
    ssh \
    git \
    nano \
    unzip \
    memcached \
    libpcre3-dev \
    zlib1g \
    zlib1g-dev \
    libpcre3-dev \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libmcrypt-dev \
    libpng12-dev \
    libcurl4-nss-dev \
    libmemcached-dev \
    libssl-dev \
    pkg-config \
    libpq-dev \
    libedit-dev \
    php5-mysql

RUN echo "Europe/Kiev" | tee /etc/timezone && \
    dpkg-reconfigure --frontend noninteractive tzdata

RUN docker-php-ext-install zip
RUN docker-php-ext-install mbstring
RUN docker-php-ext-install pdo
RUN docker-php-ext-install pdo_mysql
RUN docker-php-ext-install json
RUN docker-php-ext-install curl
RUN docker-php-ext-install mysqli
RUN printf "\n" | pecl install -o -f xdebug
RUN rm -rf /tmp/pear

RUN docker-php-ext-enable zip
RUN docker-php-ext-enable mbstring
RUN docker-php-ext-enable pdo
RUN docker-php-ext-enable pdo_mysql
RUN docker-php-ext-enable json
RUN docker-php-ext-enable curl
RUN docker-php-ext-enable mysqli
RUN docker-php-ext-enable xdebug

COPY ./php.ini /usr/local/etc/php/
COPY ./www.conf /usr/local/etc/php/

RUN apt-get install wget -y
RUN apt-get purge -y g++ && \
    apt-get autoremove -y && \
    rm -r /var/lib/apt/lists/* && \
    rm -rf /tmp/*

VOLUME /var/lib/mysql

EXPOSE 9000
CMD ["php-fpm"]