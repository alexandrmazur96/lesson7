<?php

namespace App\Http\Controllers\Api;

use App;
use App\Models\User;
use App\Services\MailService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;

class SignUpController extends Controller
{
    public function signUpUser(Request $request)
    {
        $requestParams = $request->only(
            'email',
            'first_name',
            'last_name',
            'password'
        );

        $email = ['email' => $request->input('email')];

        $requestValidationParams = [
            'email' => 'required|email|min:6',
            'first_name' => 'max:50|min:3',
            'last_name' => 'max:50|min:3',
            'password' => 'required|min:8'
        ];

        $emailExistsValidationParam = [
            'email' => 'unique:emails,email'
        ];

        $emailExistsValidator = Validator::make(
            $email,
            $emailExistsValidationParam
        );

        $requestParamsValidator = Validator::make(
            $requestParams,
            $requestValidationParams
        );

        if ($requestParamsValidator->fails()) {
            $response = new Response([], 400);
            $response->addParam('message', 'Invalid params passed');
            return $response->toJson();
        }

        if($emailExistsValidator->fails()){
            $response = new Response([], 302);
            $response->addParam('message', 'User exists');
            return $response->toJson();
        }

        $userParams = [
            'first_name' => $requestParams['first_name'],
            'last_name' => $requestParams['last_name'],
            'admin' => 0
        ];

        $user = new User($userParams);
        $user->save();

        $email = $user->emails()->create([
            'user_id' => $user->id,
            'email' => $requestParams['email'],
            'main' => true
        ]);

        $email->password()->create([
            'email_id' => $email->id,
            'password' => bcrypt($requestParams['password'])
        ]);

        (new MailService)->sendRegistrationMail($user->id);

        return response('User registered', 200);
    }
}
